import pandas as pd
import numpy as np

szereg = pd.Series([5, 7, 8, 10, -1, -1, 0])
print(szereg)

# inny typ danych
szereg2 = pd.Series([5.0, 7, 8, 10, -1, -1, 0])
print(szereg2)

# nie zawsze indeksowanie od zera
szereg3 = pd.Series([5, 7, 8, 10, -1, -1, 0], index=[100, 110, 120, 130, 140, 150, 160])
print(szereg3)

# Proste - jak w listach
print(szereg[0])
szereg[0] = 4
szereg[10] = 40

print(szereg.sum())
print(szereg.mean())

# Obejrzyj szereg
print(len(szereg3))   # tak jak w liście
print(szereg3.head()) # można więcej wierszy

# Podglądanie danych
print(szereg3.value_counts())
print(szereg3.describe())

print(szereg > 0)
print((szereg > 0).sum())     # ile dokładnie elementów spełnia warunek

# Zapis / odczyt do CSV  (to samo DataFrame)
szereg.to_csv(r'Arkusz1.csv') # zmień ścieżkę
szereg_nowy = pd.read_csv(r'Arkusz1.csv') # zmień ścieżkę

# .all(), .any()
print((szereg > 0).all())

# wartości NA (brak danych)
szereg.isna()
szereg.notna()

szereg.isna().any()    # czy jest chociaż 1 obserwacja "pusta"


# pd.DataFrame()


