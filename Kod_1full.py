import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics
import os

# change the directory if needed
# os.chdir()

data = pd.DataFrame()
data = pd.read_csv('bank-full.csv', sep = ";")

print(data.head())

# data preprocess
data['y'] = (data['y'] == "yes").astype(int) # zmienna objasniana
categorical_cols = ['month', 'job', 'marital']
for i in categorical_cols:
    data[i] = data[i].astype('category')
    xtemp = pd.get_dummies(data[i])
    # put dummies to Dataframe
    for jname in xtemp.columns:
        nazwanowa = i + "_" + jname
        data[nazwanowa] = xtemp[jname]


# to samo co w petli powyzej mozna inaczej
# from sklearn.feature_extraction import DictVectorizer
# dvec = DictVectorizer(sparse=False)
# X = dvec.fit_transform(df.transpose().to_dict().values())

data['housing_binary'] = (data['housing'] == "yes").astype(int)
data['loan_binary'] = (data['loan'] == "yes").astype(int)

# Podzielimy zbior na trening. i test. (podzial jest zupelnie od czapy, to tylko przyklad)
data_train = data[ : ][ 2000:41000 ]
data_test = pd.concat([data[:][ 0:2000 ], data[:][ 41000:45211 ]])

# ALTERNATYWNIE random sample split (sklearn)

print("Dane uczace " + str(len(data_train)) + " obs., a testowe " + str(len(data_test)) + " obs.")

exogenous_vars = ['age', 'marital_divorced', 'marital_married', 'housing_binary', 'loan_binary']
# dodaj sezonowosc miesieczna
exogenous_vars += ['month_apr', 'month_aug', 'month_dec', 'month_feb', 'month_jan',
                   'month_jul', 'month_jun', 'month_mar', 'month_may', 'month_nov', 'month_oct'] # bez 'month_sep'

# logit
logit_model = LogisticRegression()
logit_model.fit(data_train[exogenous_vars], data_train['y'])
# logit_model2 to ŹLE - nie trenujemy modelu na zbiorze testowym!
# logit_model2 = LogisticRegression()
# logit_model2.fit(data_test[exogenous_vars], data_test['y'])

data_train["y_pred_logit"] = logit_model.predict(data_train[exogenous_vars])
# poniżej ŹLE - predykcję na zbiór testowy robimy poza próbę, modelem
# który był trenowany tylko na zbiorze tesowym
#data_test["y_pred_logit"] = logit_model2.predict(data_test[exogenous_vars])
data_test["y_pred_logit"] = logit_model.predict(data_test[exogenous_vars])

print("Logit\n")
print( metrics.confusion_matrix(data_train["y"], data_train["y_pred_logit"]) )
print( metrics.classification_report(data_train["y"], data_train["y_pred_logit"]) )
print("Test sample (logit):")
print( metrics.confusion_matrix(data_test["y"], data_test["y_pred_logit"]) )
print( metrics.classification_report(data_test["y"], data_test["y_pred_logit"]) )
#print( logit_model.score(data_train["y"], data_train["y_pred_logit"]) )

# naiwny Bayes
nbayes_model = GaussianNB()
nbayes_model.fit(data_train[exogenous_vars], data_train['y'])
data_train["y_pred_nabayes"] = nbayes_model.predict(data_train[exogenous_vars])
data_test["y_pred_nabayes"] = nbayes_model.predict(data_test[exogenous_vars])
print("Nawny klasyfikator Bayesa\n")
print( metrics.confusion_matrix(data_train["y"], data_train["y_pred_nabayes"]) )
print( metrics.classification_report(data_train["y"], data_train["y_pred_nabayes"]) )
print("Test sample (NBayes):")
print( metrics.confusion_matrix(data_test["y"], data_test["y_pred_nabayes"]) )
print( metrics.classification_report(data_test["y"], data_test["y_pred_nabayes"]) )


# LDA (samodzielnie)



